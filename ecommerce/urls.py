from django.conf.urls import patterns, include, url
import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ecommerce.views.home', name='home'),
    # url(r'^ecommerce/', include('ecommerce.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^',include('ecommerce.apps.home.urls')),
    url(r'^',include('ecommerce.apps.vendas.urls')),
    #url(r'^',include('ecommerce.apps.agenda.urls')),
    url(r'^',include('ecommerce.apps.webServices.wsProductos.urls')),
    # url(r'^',include('ecommerce.apps.vendas.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/(?P<path>.*)$','django.views.static.serve',{'document_root':settings.MEDIA_ROOT}),
)
