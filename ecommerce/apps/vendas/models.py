from django.db import models

class itemagenda(models.Model):
	data = models.DateField()
	hora = models.TimeField()
	titulo = models.CharField(max_length=100)
	descricao = models.TextField()
	status		= models.BooleanField(default=True)

	def __unicode__(self):
		return self.titulo
		
class cliente(models.Model):
	nome		= models.CharField(max_length=200)
	sobrenome	= models.CharField(max_length=200)
	status		= models.BooleanField(default=True)

	def __unicode__(self):
		nomeCompleto = "%s %s"%(self.nome,self.sobrenome)
		return nomeCompleto

class categoriaProducto(models.Model):
	nome		= models.CharField(max_length=200)
	descricao   = models.TextField(max_length=300)

	def __unicode__(self):
		return self.nome
		

class produto(models.Model):

	def url(self,filename):
		ruta = "MultimediaData/Producto/%s/%s"%(self.nome,str(filename))
		return ruta

	def thumbnail(self):
		return '<a href="/media/%s"><img src="/media/%s"	width=50px heigth=50px/></a>'%(self.imagen,self.imagen)
	
	thumbnail.allow_tags = True	

	nome		= models.CharField(max_length=100)
	descricao	= models.TextField(max_length=300)
	status		= models.BooleanField(default=True)
	imagen 		= models.ImageField(upload_to=url,null=True,blank=True)
	preco		= models.DecimalField(max_digits=6,decimal_places=2)
	stock		= models.IntegerField()
	categorias  = models.ManyToManyField(categoriaProducto,null=True,blank=True)
	
	
	def __unicode__(self):
		return self.nome

