from django.shortcuts import render_to_response
from django.template import RequestContext
from ecommerce.apps.vendas.forms import addProductForm
from ecommerce.apps.vendas.models import produto
from django.http import HttpResponseRedirect

from django.core.paginator import Paginator,EmptyPage,InvalidPage


def add_product_view(request):
	info = "iniciado"
	if request.method == "POST":
		form = addProductForm(request.POST,request.FILES)
		if form.is_valid():
			add = form.save(commit=False)
			add.status = True
			add.save() # Guardamos as informacoes
			form.save_m2m() # Guarda as relacoesde ManyToMany
			info = "Salvo com sucesso!"
			return HttpResponseRedirect('/producto/%s'%add.id)
	else:
		form = addProductForm()
	ctx = {'form':form,'informacion':info}
	return render_to_response('vendas/addProducto.html',ctx,context_instance=RequestContext(request)) 

def edit_product_view(request,id_prod):
	info = "iniciado"
	prod = produto.objects.get(pk=id_prod)
	if request.method == "POST":
		form = addProductForm(request.POST,request.FILES,instance=prod)
		if form.is_valid():
			edit_prod = form.save(commit=False)
			form.save_m2m()
			edit_prod.status = True
			edit_prod.save() # Guardamos o objeto
			info = "Correto"
			return HttpResponseRedirect('/producto/%s/'%edit_prod.id)
	else:
		form = addProductForm(instance=prod)
	ctx = {'form':form,'informacion':info}
	return render_to_response('vendas/editProducto.html',ctx,context_instance=RequestContext(request))

"""
def add_product_view(request):
	info = "Inicializando" 
	if request.user.is_authenticated():
		if request.method == "POST":
			form = addProductForm(request.POST,request.FILES)
			if form.is_valid():
				nome = form.cleaned_data['nome']
				descricao = form.cleaned_data['descricao']
				imagen = form.cleaned_data['imagen'] # Esto se obtiene con el request.FILES
				preco = form.cleaned_data['preco']
				stock = form.cleaned_data['stock']
				p = produto()
				if imagen: # Generamos una pequenia validacion.
					p.imagen = imagen
				p.nome 		=  nome
				p.descricao 	= descricao
				p.preco 		= preco
				p.stock 		= stock
				p.status = True
				p.save() # Guardar la informacion
				info = "Se guardo satisfactoriamente!!!!!"
			else:
				info = "informacion con datos incorrectos"			
		form = addProductForm()
		ctx = {'form':form, 'informacion':info}
		return render_to_response('vendas/addProducto.html',ctx,context_instance=RequestContext(request))
	else:
		return HttpResponseRedirect('/')

"""
"""
def edit_product_view(request,id_prod):
	p = produto.objects.get(id=id_prod)
	if request.method == "POST":
		form = addProductForm(request.POST,request.FILES)
		if form.is_valid():
				nome = form.cleaned_data['nome']
				descricao=form.cleaned_data['descricao']
				imagen = form.cleaned_data['imagen'] # Esto se obtiene con el request.FILES
				preco = form.cleaned_data['preco']
				stock = form.cleaned_data['stock']
				p.nome 		=  nome
				p.descricao 	= descricao
				p.preco 		= preco
				p.stock 		= stock
				if imagen:
					p.imagen = imagen
				p.save() # Guardar la informacion
				info = "Se guardo satisfactoriamente!!!!!"
				return HttpResponseRedirect('/producto/%s'%p.id)
	if request.method == "GET":
		form = addProductForm(initial={
									'nome':p.nome,
									'descricao':p.descricao,
									'preco':p.preco,
									'stock':p.stock,
			})
	ctx = {'form':form,'producto':p}
	return render_to_response('vendas/editProducto.html',ctx,context_instance=RequestContext(request))		
"""