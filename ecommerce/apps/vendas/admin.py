from django.contrib	import admin
from ecommerce.apps.vendas.models import cliente,produto,categoriaProducto,itemagenda

class productoAdmin(admin.ModelAdmin):
	list_display = ('nome','thumbnail','preco','stock')
	list_filter = ('nome','preco')
	search_fields = ['nome','preco']
	fields = (('nome','descricao'),('preco','stock','imagen'),'categorias','status')


		
admin.site.register(cliente)
admin.site.register(produto,productoAdmin)
admin.site.register(categoriaProducto)
admin.site.register(itemagenda)


