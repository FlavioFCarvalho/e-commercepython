from django import forms
from ecommerce.apps.vendas.models import produto,itemagenda

class addProductForm(forms.ModelForm):
	class Meta:
		model   = produto
		exclude = {'status',}

class FormItemAgenda(forms.ModelForm):
	data = forms.DateField(widget=forms.DateInput(format='%d/%m/%Y'),
	input_formats=['%d/%m/%Y', '%d/%m/%y'])

	class Meta:
		model = itemagenda
		fields = ('titulo', 'data', 'hora', 'descricao')		
				
	

"""
class addProductForm(forms.Form):
	nome		= forms.CharField(widget=forms.TextInput())
	descricao 	= forms.CharField(widget=forms.TextInput())
	imagen      = forms.ImageField(required=False)
	preco		= forms.DecimalField(required=True)
	stock		= forms.IntegerField(required=True)
	
	def clean(self):
		return self.cleaned_data
		"""